import { Component, OnInit, AfterViewChecked, AfterViewInit, AfterContentChecked } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { CommunicationService } from './commonFolder/services/communication.service';

@Component({
  selector: 'indexApp',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterContentChecked {
  title = 'batchTwo';
  stream : any;
  subscription : any;

  showImporant : any = false;

  menus = [
    {
      title : 'Basics of RXJS',
      link : 'basics-rxjs'
    },
    {
      title : 'Operators',
      link : 'operators'
    },
    {
      title : 'Subject',
      link : 'subject'
    },
  ]
//Promises only -
  result = new Promise((resolve, reject) =>{
        setTimeout(() => {
          let data = 3;
            if(data){
              resolve(1)
              resolve(2)
              resolve(3)
            }else{
              reject('DATA NOT FOUND')
            }
        }, 3000);
  })

  constructor(private readonly comService : CommunicationService){

  }

  ngOnInit(): void {

      // this.result.then(res=> console.log(res, "Promise Data"))
      // .catch(err => console.log(err))

      // observable 
      let channel = new Observable( res =>{ 
        setTimeout(() => {  
          res.next(" 1 New Video Launched")
        }, 2000);
        setTimeout(() => {  
            res.next(" 2 New Video Launched")
        }, 6000);
        setTimeout(() => {  
            res.next(" 3 New Video Launched")
        }, 6500);
        setTimeout(() => {  
            res.next(" 3 New Video Launched")
        }, 7000);
      })

     this.subscription = channel.subscribe(post=>{
        // console.log(post);
        
      })
      setTimeout(() => {
          this.subscription.unsubscribe();
      }, 6500);
  }

  ngAfterContentChecked(): void {
      this.comService.dataService.subscribe(res=>{
        this.showImporant = res;
      })
  }
}
