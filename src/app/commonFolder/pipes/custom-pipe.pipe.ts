import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertIntoSpecificCurr'
})
export class CustomPipePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    console.log(value,"this is value");
    console.log(args,"This is args");
    

    if(args[1].isShow == true){
      switch (args[0]) {
        case 'EUR':
          return "€" + value / 88.58;
          case 'USD':
          return "$" + value / 82;
        default:
          return value
      }
    }
    return value;
  }
}
