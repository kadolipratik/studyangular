import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, ReplaySubject, AsyncSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

 dataServiceSubject = new Subject<boolean>();

 dataService = new BehaviorSubject<boolean>(false);

 videoStream = new ReplaySubject<string>(2,5000);

 asyncStream = new AsyncSubject<any>();

  constructor() { }
}
