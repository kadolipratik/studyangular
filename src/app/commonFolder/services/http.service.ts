import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor() { }

  getData(){
    return of({"name" : "Angular", "date" : "12/12/12"});
  }
}
