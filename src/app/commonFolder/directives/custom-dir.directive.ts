import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[hideElement]'
})
export class CustomDirDirective implements OnInit {

  @Input('hideElement') text : any = '';

  constructor(private readonly el : ElementRef) { }

  ngOnInit(): void {
    console.log(this.el.nativeElement);
    console.log(this.text,"this is text");
    
      if(this.text.name == 'pratik'){
        this.el.nativeElement.style.display = 'none';
      } 
  }
}
