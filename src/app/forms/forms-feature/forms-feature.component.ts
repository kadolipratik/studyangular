import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { checkForbiddenValue, checkPasswords } from './valueCheck.validator';


@Component({
  selector: 'app-forms-feature',
  templateUrl: './forms-feature.component.html',
  styleUrls: ['./forms-feature.component.css']
})
export class FormsFeatureComponent implements OnInit {

reactiveLoginForm = new FormGroup({
  username : new FormControl(''),
  password : new FormControl('')
})

doNotMatch = false;
  constructor( private fb : FormBuilder) { 

    this.nestedForm = this.fb.group({
      name: '',
      emails: this.fb.array([
        this.fb.group({
          address: '',
          verified: false,
          phones: this.fb.array([
            this.fb.group({
              number: '',
              type: ''
            })
          ])
        })
      ])
    });
  }

 formArr : any;
  regex = /admin/;
  basicInfo = this.fb.group({
    name : ['',[Validators.required,Validators.minLength(3),checkForbiddenValue(this.regex)]],
    password : [''],
    cnPassword : [''],
    address : [''],
    age : [''],
    edu : [''],
    number : [''],
    email : [''],
    enableUpdates : [false],
    additionalNumbers : this.fb.array([]),
    additionalInfo : this.fb.group({
      hobbies : [''],
      website : [''],
      techSkills : ['']
    }),
  })

  get additionalNumbers(){
    return this.basicInfo.get('additionalNumbers') as FormArray;
  }

  get email(){
    return this.basicInfo.get('email');
  }

  addNumber(){
    this.additionalNumbers.push(this.fb.control(''));
    console.log(this.basicInfo,"this is controls");
  }

  ngOnInit(): void {
    // console.log(this.basicInfo.get('name'));
  
    // get form data from server by calling api 
    // api -> serverData 
    // let serverData = { "subject": "Angular", "postAdress": "Pune", "age": "23", "edu": "abc", "ph": "123" }
    // this.basicInfo.patchValue({
    //   name : serverData.subject,
    //   address : serverData.postAdress,
    //   number : serverData.ph
    // });
  //   this.basicInfo.patchValue({
  //     "name": "23423",
  //     "address": "234",
  //     "age": 23423,
  //     "edu": "32423",
  //     "number": 23423,
  //     "additionalInfo": {
  //         "hobbies": "234",
  //         "website": "234",
  //         "techSkills": "234"
  //     }
  // })
  // let a = this.basicInfo.get('additionalNumbers') as FormArray;
  // a.push(this.fb.control(['232323']))
  // a.push(this.fb.control('2323'))
  // console.log(this.basicInfo);
  this.basicInfo.get('enableUpdates')?.valueChanges.subscribe(value =>{
    console.log(value,"this is subscribed value"); 
    if(value == true){
    this.email?.setValidators(Validators.required);
    }else{
      this.email?.clearValidators();
    }
    this.email?.updateValueAndValidity();
    console.log(this.email,"this is updated email control")
  })
  this.basicInfo.get('edu')?.valueChanges.subscribe(value =>{
    console.log(value,"this is subscribed value"); 
  })
}

  check(){
    console.log(this.basicInfo.get('edu')?.value)
  }

  loginDetails( formData : any ){
    console.log( formData, "This is formData");   
  }

  reactiveLoginDetails( formValue : any ){
    console.log( formValue, "This is rective formData");   
  }

  submitBasicInfoForm( basicInfoValues : any){
    console.log( basicInfoValues.value, "This is rective formData"); 
  }

  checkPassword(){
    console.log("Triggered");
    
    let password = this.basicInfo.get('password');
    let cnPassword = this.basicInfo.get('cnPassword');
    console.log(password?.dirty, " this is value passsword");
    console.log(cnPassword?.dirty, " this is value passsword");
    
    if(password?.value && cnPassword?.value){
      console.log("In");
      
      if( password.value != cnPassword.value ){
        this.doNotMatch = true;
      }else{
        this.doNotMatch = false;
      }
    }

  // console.log(this.basicInfo.);
  
    // console.log(password,"this is password control")
    // console.log(cnPassword,"this is cn password control")
  }

  nestedForm: FormGroup;
  get emails() {
    return this.nestedForm.get('emails') as FormArray;
  }

  getPhonesFormGroup(index: number) {
    return (this.emails.controls[index] as FormGroup).get('phones') as FormArray;
  }

  addEmail() {
    this.emails.push(this.fb.group({
      address: '',
      verified: false,
      phones: this.fb.array([
        this.fb.group({
          number: '',
          type: ''
        })
      ])
    }));
  }

  removeEmail(index: number) {
    this.emails.removeAt(index);
  }

  addPhone(emailIndex: number) {
    this.getPhonesFormGroup(emailIndex).push(this.fb.group({
      number: '',
      type: ''
    }));
  }

  removePhone(emailIndex: number, phoneIndex: number) {
    this.getPhonesFormGroup(emailIndex).removeAt(phoneIndex);
  }
}
