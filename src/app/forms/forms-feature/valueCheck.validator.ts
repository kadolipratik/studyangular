import { AbstractControl, ValidatorFn } from "@angular/forms";

export function checkForbiddenValue(regex: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        let check = regex.test(control.value); // admin -> true
        return check ? { 'forbidden': { value: control.value } } : null;
    }
}

export function checkPasswords(control:AbstractControl){
    let password = control.get('password');
    let cnPassword = control.get('cnPassword');
    return password?.value && cnPassword?.value && password?.value != cnPassword?.value ? {'doNotMatch' : true} : null;
}


