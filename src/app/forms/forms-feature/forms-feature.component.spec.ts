import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsFeatureComponent } from './forms-feature.component';

describe('FormsFeatureComponent', () => {
  let component: FormsFeatureComponent;
  let fixture: ComponentFixture<FormsFeatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormsFeatureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
