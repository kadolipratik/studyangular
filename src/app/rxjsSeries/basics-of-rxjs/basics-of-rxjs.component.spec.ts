import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicsOfRxjsComponent } from './basics-of-rxjs.component';

describe('BasicsOfRxjsComponent', () => {
  let component: BasicsOfRxjsComponent;
  let fixture: ComponentFixture<BasicsOfRxjsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasicsOfRxjsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicsOfRxjsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
