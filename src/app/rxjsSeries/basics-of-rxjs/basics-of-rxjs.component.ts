import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { from, fromEvent, interval, of, Subscription, timer, Observable } from 'rxjs';

@Component({
  selector: 'app-basics-of-rxjs',
  templateUrl: './basics-of-rxjs.component.html',
  styleUrls: ['./basics-of-rxjs.component.css']
})
export class BasicsOfRxjsComponent implements OnInit , AfterViewInit{

 @ViewChild('subscribe') subscribe : any;
 @ViewChild('unsubscribe') unsubscribe : any;

 stopInterval: any;
 stopTimer:any;
 unsubscribeBtn:any;
  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {

    console.log("hii");
    
    //   let thisisPromise = new Promise( (resolve, reject)=>{

    //     let serverData = 1;
    //    setTimeout(() => {
    //     if(serverData == 1){
    //       resolve("Promise Resolved")
    //       resolve("Promise Resolved 2")
    //     }else{
    //       reject("Promise Reject")
    //     }
    //    }, 3000);
    //   });

    //  thisisPromise.then(resolveData =>{
    //   console.log(resolveData);
      
    //  }).catch( err =>{
    //   console.log(err);
      
    //  })

    //  let channel = new Observable( obs=>{ //1sec
    //   setTimeout(() => {
    //     obs.next("event trigger zhalt ") //video publish
    //   }, 1000);
    //   setTimeout(() => {
    //     obs.next(2)
    //   }, 2000);
    //   setTimeout(() => {
    //     obs.next(3) //emit 
    //   }, 3000);
    //   setTimeout(() => {
    //     obs.next(4)
    //   }, 4000);
    //   setTimeout(() => {
    //     obs.next(5)
    //   }, 5000);
    //  });
    //  this.unsubscribeBtn = channel.subscribe( videos=>{ //1sec 
    //       console.log(videos);
    //  });
    //  setTimeout(() => { // 1sec
    //   this.unsubscribeBtn.unsubscribe() 
    //  }, 3000);
     
    // Interval feature
    let oneSecInterval = interval(1000);


    let fiveSecDelayAndOneSecInterval = timer(5000,1000);

    // this.stopInterval = oneSecInterval.subscribe(res=>{

    //   console.log("Video published ", res);
    //   // if(res == 5){
    //   //   this.stopInterval.unsubscribe();
    //   // }
    //   // // fromEvent feature
    //   fromEvent(this.unsubscribe.nativeElement,'click').subscribe(res=>{
    //     this.stopInterval.unsubscribe();
    //   })
    // })

    // this.stopTimer = fiveSecDelayAndOneSecInterval.subscribe(res=>{

    //   console.log("Video published " , res+1);

    //   // fromEvent feature
    //   fromEvent(this.unsubscribe.nativeElement,'click').subscribe(res=>{
    //     this.stopTimer.unsubscribe()
    //   })  
    // })


    // fromEvent(this.subscribe.nativeElement,'click').subscribe(res=>{
    //     oneSecInterval.subscribe(res=>{

    //       console.log("Video published " + count++);
    // })
    // })

  }



  // channel - 1------1--------1---------1-------1--------1----------1------1---------1
  // person  -                subscribe -1-------1--------1-unsubscribe ------------------
}
