import { Component, OnInit } from '@angular/core';
import { CommunicationService } from 'src/app/commonFolder/services/communication.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.css']
})
export class SubjectsComponent implements OnInit {

  channelVideo: any = []
  channelVideo2: any = []
  channelVideo3:any = [];

  subscriber1Flag :any = false;
  subscriber2Flag :any = false;

  Subscription2 :any;
  Subscription3 :any;

  updatedAsyncData : any;

  constructor(private readonly comSerivce :CommunicationService) { }

  ngOnInit(): void {
    // console.log(this.subscriber2Flag);
    
    this.comSerivce.videoStream.subscribe(res=>{
        this.channelVideo.push(res)
    })

    this.comSerivce.asyncStream.subscribe(res=>{
     this.updatedAsyncData = res;
     console.log(res,"this is aftercomplete data");
     

    })

  }

  videoPublish(videoData : any){
    this.comSerivce.videoStream.next(videoData);
  }

  subscriber2(){

    if(!this.subscriber1Flag){
      this.Subscription2 = this.comSerivce.videoStream.subscribe(res=>{
        this.channelVideo2.push(res)
    })
    }else{
      // console.log("this is else");
      
      this.Subscription2.unsubscribe();
    }
    this.subscriber1Flag = !this.subscriber1Flag;
    
  }

  subscriber3(){
    if(!this.subscriber2Flag){
      this.Subscription3 = this.comSerivce.videoStream.subscribe(res=>{
        // console.log(res,"this is reult");
        
        this.channelVideo3.push(res)
    })
    }
    else{
      // console.log("this is else");
      
      this.Subscription3.unsubscribe();
    }
    this.subscriber2Flag = !this.subscriber2Flag;
    // console.log(this.subscriber2Flag,"this is flag data");
    
  }

  asyncPublish( dataValue : any){

    console.log(dataValue,"this is value");
    this.comSerivce.asyncStream.next(dataValue);
    
  }

  completeTheStream(){
      this.comSerivce.asyncStream.complete();
  }
}
