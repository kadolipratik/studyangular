import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { of, interval, from, fromEvent, timer, concat, merge } from 'rxjs';
import { distinctUntilChanged ,debounceTime, delay, filter, map, pluck, retry, retryWhen, scan, take, takeLast, takeUntil, tap, toArray, mergeAll, mergeMap, concatAll, concatMap, switchMap } from 'rxjs/operators';
import { CommunicationService } from '../../commonFolder/services/communication.service';

@Component({
  selector: 'app-operators',
  templateUrl: './operators.component.html',
  styleUrls: ['./operators.component.css']
})
export class OperatorsComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('searchInput') searchInputElement : any;

  dataStream : any;
  title : any = 'Angular';
  numbers: any;

 serverData = [
  {
    name : "pratik",
    occu : "Front end",
    gender : "male",
    id : 1,
    tech : {
      tech1 : "Angular"
    }
  },
  {
    name : "ratan",
    occu : "full stack",
    gender : "male",
    id : 2,
    tech : {
      tech1 : "aws"
    }
  },
  {
    name : "vaishnavi",
    occu : "full stack",
    gender : "female",
    id : 3,
    tech : {
      tech1 : "springboot"
    }
  },
  {
    name : "mansi",
    occu : "full stack",
    gender : "female",
    id : 4,
    tech : {
      tech1 : "java"
    }

  },
  {
    name : "shubham",
    occu : "Front end",
    gender : "male",
    id : 5,
    tech : {
      tech1 : "html"
    }
  },
 ];

 employees : any;

 mergeMapStreamData : any = [];
 concatMapStreamData : any = [];
 switchMapStreamData : any = [];

 employeeList:any;

  constructor(private readonly http : HttpClient,private readonly comService : CommunicationService) { }

  ngOnDestroy(): void {
    this.comService.dataService.next(false);
  }

  ngOnInit(): void {
  let arr: any = [];

    // subject act as a observer
    this.comService.dataService.next(true);


    // of,pipe and toArray operator - 
    let ofOperator = of(1,2,3,4,5,"Pratik");
    ofOperator.pipe(toArray()).subscribe( res=>{
      // console.log(res,"this is result");
      
      this.dataStream  = res;
    });

    let numbers = [1,2,3,4,5,6,7,8,9,10];
 
    //from, map operator
    from(this.serverData)
    .pipe(map(res=> res.name), toArray())
    .subscribe(res =>{
      // console.log("res of from => ",res)

      // this.employees = res;
    })

      //pluck operator
      from(this.serverData).pipe(pluck('tech','tech1'), toArray()).subscribe(res =>{
        // console.log("res of from => ",res)
        this.employees = res;
      })

    // Filter operator
    // example 1 : filter by gender
    from(this.serverData).pipe(filter(person => person.gender === 'female'), map(person => person.name), toArray()).subscribe( res =>{
      // console.log("RES => ", res);
      
    }) 

    // example 2 : filter by ids 
    from(this.serverData).pipe(filter(person => person.id <= 3),toArray()).subscribe( res=>{
      // console.log("res => ", res);
    })

    
    // convert promise into observable
    let promise = new Promise((resolve, reject)=>{
      let a = 11
      if( a == 10){
        resolve("RESOLVED")
      }
      reject("REJECT")
    });

    from(promise).subscribe( res=>{

        // console.log(res,"this is promise result");
    },err=>{
      // console.log(err,"this is error from promise");
    })

    // tap operator
    // example 1
    // from(this.serverData).pipe(tap( res=> console.log("tap result => ",res)),map( person => person.id),tap(res=>console.log("tap after filter result => ",res))).subscribe( res=>{
    //   console.log("res =>", res);
    // })
    
    // tap example 2
    // let dataStream = interval(500);
    
    // let subscription = dataStream.pipe(map(data =>{
    //   if( data > 10){
    //     subscription.unsubscribe();
    //   }
    //   return "data " + data;
    // })).subscribe(res=>{
    //   console.log(res,"this is main result");
    // })


     //take operator -
      let fiveSecDelayAndOneSecInterval = interval(500);
    //  fiveSecDelayAndOneSecInterval.pipe(take(5),toArray()).subscribe(res=>{
    //   console.log(res);
    //   this.numbers = res
    // })

    // let numbersData = [1,2,3,4,5,6,7,8,9,10]
    // // take last operator - 
    // from(numbersData).pipe(takeLast(4)).subscribe(res=>{
    //  console.log(res);
    //  this.numbers = res
    // })

    // take take until operator - 
    // let clickEvent= fromEvent(document,'click')
    // let delayData = timer(500,500);
    // delayData.pipe(takeUntil(clickEvent)).subscribe(res=>{
    //  console.log(res);
    // //  this.numbers = res
    // })

    // retry operation - 
    // this.http.get('https://6377a2ed0992902a250da3dc.mockapi.io/timespent/timespendonpage')
    // .pipe(retry(4)).subscribe(res =>{
    //   console.log(res);
      
    // },err=>{
    //   console.log(err);
      
    // })

    // this.http.get('https://6377a2ed0992902a250da3dc.mockapi.io/timespent/timespendonpage')
    // .pipe(retryWhen(err=> err.pipe(
    //   delay(10000),
    //   scan(retryCount =>{
    //       if(retryCount >=5){
    //         throw err
    //       }else{
    //         retryCount+=1;
    //         console.log("Retrying =>", retryCount);
    //         return retryCount;
    //       }
    //   },0)
    // ))).subscribe(res =>{
    //   console.log(res);
      
    // },err=>{
    //   console.log(err);
      
    // })

    // concat rxjs
    //  let education = interval(300).pipe(map(counter => "educational video " + (counter+1)),take(5));
    //  let comedy = interval(1500).pipe(map(counter => "comedy video " + (counter+1)),take(5));
    //  let business = interval(100).pipe(map(counter => "business video " + (counter+1)),take(5));

    //  concat(education, comedy, business).subscribe( res=>{
    //   console.log(res,"this is club of 3 streams using concat method");
    //  })


    //  merge rxjs
    //  let education = interval(2000).pipe(map(counter => "educational video " + (counter+1)),take(5));
    //  let comedy = interval(3000).pipe(map(counter => "comedy video " + (counter+1)),take(5));
    //  let business = interval(1000).pipe(map(counter => "business video " + (counter+1)),take(5));

    //  merge(business, education, comedy).subscribe( res=>{
    //   console.log(res);
    //  })


    // mergeMap 
    let streamData = from(['Angular', 'Java', 'springBoot']);
 

    //  standard map

    // streamData.pipe(map(keyword => this.getdataFromServer(keyword))).subscribe(res=>{
    //   console.log(res);
    //   res.subscribe( finalData =>{
    //     console.log(finalData,"this is final data");
        
    //   })
    // })

    //  standard map + mergeall

    // streamData.pipe(
    //   map(keyword => this.getdataFromServer(keyword)),
    //   mergeAll()
    //   ).subscribe(res=>{
    //   console.log(res);
    // })

    //  standard mergeMap

    streamData.pipe(
      mergeMap(keyword => this.getdataFromServer(keyword)),
      ).subscribe(res=>{
        this.mergeMapStreamData.push(res)
    }) 
    
    
    //  standard map + concateall

    // streamData.pipe(
    //   map(keyword => this.getdataFromServer(keyword)),
    //   concatAll()
    //   ).subscribe(res=>{
    //   console.log(res, "this is concat result");
    // })

    //  standard concatMap
    // streamData.pipe(
    //   concatMap(keyword => this.getdataFromServer(keyword)),
    //   ).subscribe(res=>{
    //     this.concatMapStreamData.push(res)
    // })


    // // switch map - 
    // streamData.pipe(
    //   switchMap(keyword => this.getdataFromServer(keyword)),
    //   ).subscribe(res=>{
    //     this.switchMapStreamData.push(res)
    // })

    this.employeeList = this.http.get('https://6377a2ed0992902a250da3dc.mockapi.io/timespent/timespendonpage');
    // empData.subscribe(res=>{
    //   console.log(res,"this is result");
    //     this.employeeList = res;
    // })
    
  }

  getdataFromServer( keyword : any ){

    return of(keyword + ' is a language').pipe(delay(2000));
  }

  ngAfterViewInit(): void {
    // console.log(this.searchInputElement,"this is my input element");
    // let searchItem = fromEvent(this.searchInputElement.nativeElement,'keyup')
    // .pipe(pluck('target','value'), debounceTime(1000), distinctUntilChanged());
    // let searchSubscription = searchItem.subscribe(searchItem =>{
    //   console.log(searchItem,"this is search Data");

    // })

    // const source$ = from([1, 2, 3, 4, 5]);

    // source$.pipe(
    //   scan((acc, value) => acc + value, 0) // Accumulate the sum starting from 0
    // ).subscribe((result) => {
    //   console.log(result);
    // });
  }
}

