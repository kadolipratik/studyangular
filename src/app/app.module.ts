import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { appRouting } from './appRoute-routing.module';
import { RoutingCheckComponent } from './routing-check/routing-check.component';
import { BasicsOfRxjsComponent } from './rxjsSeries/basics-of-rxjs/basics-of-rxjs.component';
import { OperatorsComponent } from './rxjsSeries/operators/operators.component';
import { SubjectsComponent } from './rxjsSeries/subjects/subjects.component';


@NgModule({
  declarations: [
    AppComponent,
    RoutingCheckComponent,
    BasicsOfRxjsComponent,
    OperatorsComponent,
    SubjectsComponent
  ],
  imports: [
    BrowserModule,
    appRouting,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
