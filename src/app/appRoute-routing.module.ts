
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoutingCheckComponent } from './routing-check/routing-check.component';
import { BasicsOfRxjsComponent } from './rxjsSeries/basics-of-rxjs/basics-of-rxjs.component';
import { OperatorsComponent } from './rxjsSeries/operators/operators.component';
import { SubjectsComponent } from './rxjsSeries/subjects/subjects.component';

const routes: Routes = [
  {
    path:"basics-rxjs",
    component:BasicsOfRxjsComponent
  },
  {
    path:"operators",
    component:OperatorsComponent
  },
  {
    path:"subject",
    component:SubjectsComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class appRouting { }
