import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {

  serverData = {
    isShow : false,
    name : "nodejs",
    listOfEmp : [
      {
        name : "pratik",
        deg : "Angular Dev",
        empId : 12,
        isCompleted : false,
        fontColor : 'red',
      },
      {
        name : "Ratan",
        deg : "Full stack Dev",
        empId : 23,
        isCompleted : true,
        fontColor : 'blue',
      },
      {
        name : "ajay",
        deg : "Backend Dev",
        empId : 56,
        isCompleted : false,
        fontColor : 'yellow',
      }
    ]
  }
  data = {
    name : "abc",
    "@!backenasdasdasd @data" : 0
  }

  constructor() { }

  ngOnInit(): void {
  }

}
