import { Component, OnInit } from '@angular/core';
import { HttpService } from '../commonFolder/services/http.service';


@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {

  constructor(private http : HttpService) { }

  ngOnInit(): void {
    this.getDetails();
  }

  getDetails(){
    let data = this.http.getData().subscribe(res=>{
      console.log(res);
      
    });
  }
}
