import { Component, OnInit, DoCheck } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit, DoCheck {

  courseName = 'Angular';
  nestedForm;

  toggleChild = false;
  constructor( private fb : FormBuilder) { 
    console.log("Parent constructor triggred"); 

      this.nestedForm = this.fb.group({
        name: '',
        emails: this.fb.array([
          this.fb.group({
            address: '',
            verified: false,
            phones: this.fb.array([
              this.fb.group({
                number: '',
                type: ''
              })
            ])
          })
        ])
      });
  }

  ngOnInit(): void {
    console.log("Parent oninit function triggred");
  }

  ngDoCheck(): void {
    console.log("Parent docheck function triggered");
}
get emails() {
  return this.nestedForm.get('emails') as FormArray;
}

getPhonesFormGroup(index: number) {
  return (this.emails.controls[index] as FormGroup).get('phones') as FormArray;
}

addEmail() {
  this.emails.push(this.fb.group({
    address: '',
    verified: false,
    phones: this.fb.array([
      this.fb.group({
        number: '',
        type: ''
      })
    ])
  }));
}

removeEmail(index: number) {
  this.emails.removeAt(index);
}

addPhone(emailIndex: number) {
  this.getPhonesFormGroup(emailIndex).push(this.fb.group({
    number: '',
    type: ''
  }));
}

removePhone(emailIndex: number, phoneIndex: number) {
  this.getPhonesFormGroup(emailIndex).removeAt(phoneIndex);
}

submitForm(form:any){
  console.log(form.value);
  
}



  getDataFromChildComponent(childData : any){
    let parsedData = JSON.parse(childData)
    console.log(parsedData,"this is child data into parent");
  }

}

