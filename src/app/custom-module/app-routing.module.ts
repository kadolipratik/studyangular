
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DirectivesComponent } from '../directives/directives.component';
import { PipesComponent } from './pipes/pipes.component';
import { ServicesComponent } from '../services/services.component';
import { ParentComponent } from './parent/parent.component';

const routes: Routes = [
  {
    path : "parent",
    component : ParentComponent
  },
  {
    path : "directives",
    component : DirectivesComponent
  },
  {
    path : "pipes",
    component : PipesComponent
  },
  {
    path : "services",
    component : ServicesComponent
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomAppRoutingModule { }
