import { Component, Input, OnChanges, OnInit, Output, SimpleChanges, EventEmitter, OnDestroy, DoCheck, AfterContentInit, ContentChild, AfterContentChecked, AfterViewInit, AfterViewChecked } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements
 OnInit, 
 OnDestroy, 
 OnChanges, 
 DoCheck, 
 AfterContentInit,
 AfterContentChecked,
 AfterViewInit,
 AfterViewChecked{

  @Input() name= '';
  @Output() triggerClick = new EventEmitter;

  @ContentChild('parentContent') parentContent : any;

  counter = 0;
//  interval: any;

  constructor() { 
    console.log("Child constructor triggred");
  }

  ngOnInit(): void {
    console.log("Child oninit function triggred");
    // console.log("oninit content",this.parentContent);
  //  this.interval = setInterval( ()=>{
  //       this.counter = this.counter + 1;
  //       console.log(this.counter," +count");
        
  //   },1000)
    // console.log(this.name,"this is parent data, Child oninit");
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(this.name,"this is change");
    // console.log(changes,"this is change");
    console.log("Child Onchanges function triggred")
    // console.log("onchanges content",this.parentContent);
  }

  ngDoCheck(): void {
    //one time after ng oninit()
    //on every onchanges()
      console.log("Child docheck function triggered");
      // console.log("docheck content",this.parentContent);
  }

  ngAfterContentInit(): void {
    // one time after do check()
      console.log("Child ngaftercontentInit function triggred");
      // console.log("aftercontentinit content",this.parentContent);
  }

  ngAfterContentChecked(): void {
    // one time after ngAfterContentInit()
    // every ngDoCheck()
      console.log("Child ngaftercontentChecked function triggred");
      // console.log("aftercontentinit content",this.parentContent);
  }


  ngAfterViewInit(): void {
    //one time after ngAfterContentChecked();
      console.log("Child ngafterviewInit function triggred")
  }

  ngAfterViewChecked(): void {
    //one time after ngAfterViewInit();
    //every ngAfterContentChecked();
      console.log("Child ngafterviewChecked function triggred")
  }



  // triggerParent(){
  //   console.log("This is child click");
  //   let data = {
  //     name : "Angular",
  //     duration : 3
  //   }
  //   this.triggerClick.emit(JSON.stringify(data));
  // }

  ngOnDestroy(): void {
      // clearInterval(this.interval);
      console.log("Child ondestroy function triggred");   
  }
}
