import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {

  title = 'AnGuLar' || 'Java';
  curr = 360;

  inrAmount = 1002;
  todaysDate = new Date();

  serverData = {
    isShow : true,
    name : "nodejs",
    listOfEmp : [
      {
        name : "pratik",
        deg : "Angular Dev",
        empId : 12,
        isCompleted : false,
        fontColor : 'red',
      },
      {
        name : "Ratan",
        deg : "Full stack Dev",
        empId : 23,
        isCompleted : true,
        fontColor : 'blue',
      },
      {
        name : "ajay",
        deg : "Backend Dev",
        empId : 56,
        isCompleted : false,
        fontColor : 'yellow',
      }
    ]
  }

  constructor() { }

  ngOnInit(): void {

    console.log(this.title);
    
  }

}
