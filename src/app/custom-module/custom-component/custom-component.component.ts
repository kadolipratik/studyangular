import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-custom-component',
  templateUrl: './custom-component.component.html',
  styleUrls: ['./custom-component.component.css']
})
export class CustomComponentComponent implements OnInit {

  data = 'My first Angular App';
  apiData = '123';

  twoWayBindingVariable = '';

  constructor() { }

  ngOnInit(): void {
  }

  // jit aot example function
  getStart(){
    return "Hello there !";
  }

  // event binding fucntion
  eventBinding(event :any){
    console.log("Hii there !!!!!!!!!!!!",typeof event);
  }
}
