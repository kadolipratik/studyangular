import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomComponentComponent } from './custom-component/custom-component.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesComponent } from '../directives/directives.component';
import { CustomDirDirective } from '../commonFolder/directives/custom-dir.directive';
import { PipesComponent } from './pipes/pipes.component';
import { CustomPipePipe } from '../commonFolder/pipes/custom-pipe.pipe';
import { ServicesComponent } from '../services/services.component';
import { HttpService } from '../commonFolder/services/http.service';
import { CustomAppRoutingModule } from './app-routing.module';
import { FormsFeatureComponent } from '../forms/forms-feature/forms-feature.component';
import { ChildComponent } from './child/child.component';
import { ParentComponent } from './parent/parent.component';
import { MaterialModule } from '../material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';




@NgModule({
  declarations: [
    CustomComponentComponent,
    DirectivesComponent,
    CustomDirDirective,
    PipesComponent,
    CustomPipePipe,
    ServicesComponent,
    FormsFeatureComponent,
    ChildComponent,
    ParentComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    CustomAppRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  bootstrap: [ParentComponent],
  providers: [HttpService]
})
export class CustomModuleModule { }
